import pickle
import pandas as pd
import seaborn as sea
import matplotlib.pyplot as plt
import numpy as np

def open_file(filename):
    stack_call =[]
    with open(filename, "rb") as openfile:
        while True:
            try:
                stack_call.append(pickle.load(openfile))
            except EOFError:
                break
    return stack_call


def update_dict(bot_name, stacks):
    collect = []
    times = []
    stack = []
    bot = []
    keep = 0
    for time, i in enumerate(stacks):
  #      change = i[0] - keep
#        keep = i[time]
        collect.append(i[0])
        times.append(time+1)
        stack.append(np.mean(collect))
        bot.append(bot_name)
    return times, stack, bot

#open files to be a;ter
stack_TC = open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\table_call_stack.pickle")
stack_TB =open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\table_blog_stack.pickle")
stack_TG =open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\table_general_stack.pickle")
stack_GC = open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\call_blog_general_table_stack.pickle")
stack_GB = open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\general_blog_stack.pickle")
stack_GT = open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\general_table_stack.pickle")

stackGTB = open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\general_table_blog_stack.pickle")
stackTGB = open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\table_general_blog_stack.pickle")

stackBGT = open_file("C:\\Users\Ruby\Documents\PokerBot\pokerbot\\blog_general_table_stack.pickle")

collect_stacks = {"time": [], "stack": [], "bot": []}
# timeTC, stackTC, botTC= update_dict("Table vs. Call", stack_TC[0])
timeTB, stackTB, botTB= update_dict("Table", stackTGB[0])
timeTG, stackTG, botTG= update_dict("Blogger", stackBGT[0])
# timeGT, stackGT, botGT= update_dict("General vs Table", stack_GT[0])
timeGC, stackGC, botGC= update_dict("Call", stack_GC[0])
timeGB, stackGB, botGB= update_dict("General", stackGTB[0])




collect_stacks["time"] = timeTB+timeGB+timeTG+timeGC
collect_stacks["bot"] = botTB+botGB+botTG+botGC
collect_stacks["stack"] = stackTB+stackGB+stackTG+stackGC
dataframe = pd.DataFrame.from_dict(collect_stacks)

sea.set(font_scale=3)
sea.set_style("whitegrid")
ax = sea.lineplot(x="time", y="stack", hue="bot", data=dataframe, palette="deep", legend="full")
plt.setp(ax.get_legend().get_texts(), fontsize='22') # for legend text
plt.setp(ax.get_legend().get_title(), fontsize='32') # for legend title

plt.show()