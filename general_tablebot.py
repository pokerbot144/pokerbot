
from pypokerengine.engine.hand_evaluator import HandEvaluator
from pypokerengine.players import BasePokerPlayer
from pypokerengine.utils.card_utils import _pick_unused_card, _fill_community_card, gen_cards
import numpy as np
import pickle

n = np.random.random_sample()

def get_stack_amount(uuid, seats):
    for dict in seats:
        if dict['uuid'] is uuid:
            return dict['stack']

# Estimate the ratio of winning games given the current state of the game
def estimate_win_rate(nb_simulation, nb_player, hole_card, community_card=None):
    if not community_card: community_card = []

    # Make lists of Card objects out of the list of cards
    community_card = gen_cards(community_card)
    hole_card = gen_cards(hole_card)

    # Estimate the win count by doing a Monte Carlo simulation
    win_count = sum([montecarlo_simulation(nb_player, hole_card, community_card) for _ in range(nb_simulation)])
    return 1.0 * win_count / nb_simulation


def evaluate_hand(hole_card, community_card):
    assert len(hole_card)==2 and len(community_card)==5
    hand_info = HandEvaluator.gen_hand_rank_info(hole_card, community_card)
    return {
            "hand": hand_info["hand"]["strength"],
            "strength": HandEvaluator.eval_hand(hole_card, community_card)
}

def montecarlo_simulation(nb_player, hole_card, community_card):

    # Do a Monte Carlo simulation given the current state of the game by evaluating the hands
    community_card = _fill_community_card(community_card, used_card=hole_card + community_card)

    unused_cards = _pick_unused_card((nb_player - 1) * 2, hole_card + community_card)
    opponents_hole = [unused_cards[2 * i:2 * i + 2] for i in range(nb_player - 1)]
    opponents_score = [HandEvaluator.eval_hand(hole, community_card) for hole in opponents_hole]
    my_score = HandEvaluator.eval_hand(hole_card, community_card)
    return 1 if my_score >= max(opponents_score) else 0


def convert_win_rate(win_rate,):
    if win_rate > 0.5:
        #raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
        if win_rate > 0.85:
            state = "high"
        elif win_rate > 0.75:
            # If it is likely to win, then raise by the minimum amount possible
            state = "mid_high"
        else:
            state = "mid"
    else:
        state = "low"
    return state


def get_table_values(table, win_rate):
    nums = [table[win_rate][action] for action in table[win_rate]]
    acts = [action for action in table[win_rate]]
    return (acts[nums.index(max(nums))])

def get_action_amount(win_rate, check, valid_actions,table, action_counts):
    raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
    can_call = len([item for item in valid_actions if item['action'] == 'call']) > 0
    if can_call:
        # If so, compute the amount that needs to be called
        call_amount = [item for item in valid_actions if item['action'] == 'call'][0]['amount']
    else:
        call_amount = 0
    amount = 0
    get_amount = {"raise": raise_amount_options['max'], "call": call_amount , "fold": 0}
    # Check whether it is possible to call
    action = get_table_values(table, win_rate )

    if amount is None:
        items = [item for item in valid_actions if item['action'] == action]
        amount = items[0]['amount']
    action_counts[win_rate][action] += 1
    check[win_rate] = (action)
    return action, get_amount[action], check, action_counts


def restart_check():
    check = {"low ": (),
                   "mid": (),
                  "mid_high" : (),
                    "high":
         ()

     }
    return check

def initialize_table():

    table = {"low":
                                        {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                "mid":
                                        {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                "mid_high":
                                        {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                "high":
                                        {"fold": np.random.random_sample() ,
                                           "raise": np.random.random_sample(),
                                           "call": np.random.random_sample()}

                      }
    return table
class GeneralBot(BasePokerPlayer):
    def __init__(self):
        super().__init__()
        self.wins = 0
        self.losses = 0
        self.initial_stack = 0
        self.table = initialize_table()
        self.check = restart_check()
        self.counts = 0
        self.action_counts = {"low": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid_high": {"fold": 0,
                                               "raise": 0,
                                               "call": 0},
                                  "high": {"fold": 0 ,
                                           "raise": 0,
                                           "call": 0}

                              }

    def declare_action(self, valid_actions, hole_card, round_state):
        # Estimate the win rate
        win_rate = estimate_win_rate(100, self.num_players, hole_card, round_state['community_card'])
        state = convert_win_rate(win_rate)
        action, amount, self.check, self.action_counts = get_action_amount(state,  self.check, valid_actions, self.table, self.action_counts)
        return action, amount

    def receive_game_start_message(self, game_info):
        self.num_players = game_info['player_num']



    def receive_round_start_message(self, round_count, hole_card, seats):
        self.initial_stack = get_stack_amount(self.uuid, seats)
        pass

    def receive_street_start_message(self, street, round_state):
        pass

    def receive_game_update_message(self, action, round_state):
        pass

    def receive_round_result_message(self, winners, hand_info, round_state):
        #keeping track of wins/losses
        is_winner = self.uuid in [item['uuid'] for item in winners]
        orgwins = self.wins
        self.wins += int(is_winner)
        self.losses += int(not is_winner)
        stack = get_stack_amount(self.uuid, round_state['seats'])

        #figures out how much to adjust training based on diff for stacks
        diff_stack = stack - self.initial_stack
        if orgwins < self.wins:
            diff_stack = abs(diff_stack)

        #adjusting table amounts and keep tracking of global variabels
        for win_rate in self.check.keys():
                if self.check[win_rate] is not ():
                    action = self.check[win_rate] # dataframe --> {fold:(action took)}
                    self.table[win_rate][action] += diff_stack
        self.check = restart_check()
        self.counts += 1

        #to save for visualization
        with open("GTB_endtable.pickle", "wb+") as out:
            pickle.dump(self.table, out)
        with open("GTB_counts.pickle", "wb+") as out:
            pickle.dump(self.action_counts, out)


def setup_ai():
    return GeneralBot()
