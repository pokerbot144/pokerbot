
from pypokerengine.engine.hand_evaluator import HandEvaluator
from pypokerengine.players import BasePokerPlayer
from pypokerengine.utils.card_utils import _pick_unused_card, _fill_community_card, gen_cards
import tensorflow as tf
from TFSessionManager import TFSessionManager as TFSN
import keras
import numpy as np
import pickle
import pandas as pd



class QNetwork:
    """
    Contains a TensorFlow graph which is suitable for learning the Tic Tac Toe Q function
    """

    def __init__(self, name: str, learning_rate: float):
        """
        Constructor for QNetwork. Takes a name and a learning rate for the GradientDescentOptimizer
        :param name: Name of the network
        :param learning_rate: Learning rate for the GradientDescentOptimizer
        """
        self.learningRate = learning_rate
        self.name = name
        self.input_positions = None
        self.target_input = None
        self.q_values = None
        self.probabilities = None
        self.train_step = None
        self.build_graph(name)

    def add_dense_layer(self, input_tensor: tf.Tensor, output_size: int, activation_fn=None,
                        name: str = None) -> tf.Tensor:
        """
        Adds a dense Neural Net layer to network input_tensor
        :param input_tensor: The layer to which we should add the new layer
        :param output_size: The output size of the new layer
        :param activation_fn: The activation function for the new layer, or None if no activation function
        should be used
        :param name: The optional name of the layer. Useful for saving a loading a TensorFlow graph
        :return: A new dense layer attached to the `input_tensor`
        """
      #  keras.
        init_op = tf.global_variables_initializer()
        return tf.layers.dense(input_tensor, output_size, activation=activation_fn,
                               kernel_initializer=tf.contrib.layers.variance_scaling_initializer(),
                               name=name)

    def build_graph(self, name: str):
        """
        Builds a new TensorFlow graph with scope `name`
        :param name: The scope for the graph. Needs to be unique for the session.
        """
        with tf.variable_scope(name):
            self.input_positions = tf.placeholder(tf.float32, shape=(None, 16* 3), name='inputs')

            self.target_input = tf.placeholder(tf.float32, shape=(None,3), name='targets')
            net = self.input_positions
           # init_op = tf.global_variables_initializer()
            net = self.add_dense_layer(net, 3 * 9, tf.nn.relu)

            self.q_values = self.add_dense_layer(net, 3, name='q_values')

            self.probabilities = tf.nn.softmax(self.q_values, name='probabilities')
            #init_op = tf.global_variables_initializer()
            mse = tf.losses.mean_squared_error(predictions=self.q_values, labels=self.target_input)

            self.train_step = tf.train.GradientDescentOptimizer(learning_rate=self.learningRate).minimize(mse, name='train')
            with tf.Session() as sess:
                sess.run(tf.global_variables_initializer())



"""
END OF QNETWORK
"""




n = np.random.random_sample()

def get_stack_amount(uuid, seats):
    for dict in seats:
        if dict['uuid'] is uuid:
            return dict['stack']

# Estimate the ratio of winning games given the current state of the game
def estimate_win_rate(nb_simulation, nb_player, hole_card, community_card=None):
    if not community_card: community_card = []

    # Make lists of Card objects out of the list of cards
    community_card = gen_cards(community_card)
    hole_card = gen_cards(hole_card)

    # Estimate the win count by doing a Monte Carlo simulation
    win_count = sum([montecarlo_simulation(nb_player, hole_card, community_card) for _ in range(nb_simulation)])
    return 1.0 * win_count / nb_simulation


def evaluate_hand(hole_card, community_card):
    assert len(hole_card)==2 and len(community_card)==5
    hand_info = HandEvaluator.gen_hand_rank_info(hole_card, community_card)
    return {
            "hand": hand_info["hand"]["strength"],
            "strength": HandEvaluator.eval_hand(hole_card, community_card)
}

def montecarlo_simulation(nb_player, hole_card, community_card):

    # Do a Monte Carlo simulation given the current state of the game by evaluating the hands
    community_card = _fill_community_card(community_card, used_card=hole_card + community_card)

    unused_cards = _pick_unused_card((nb_player - 1) * 2, hole_card + community_card)
    opponents_hole = [unused_cards[2 * i:2 * i + 2] for i in range(nb_player - 1)]
    opponents_score = [HandEvaluator.eval_hand(hole, community_card) for hole in opponents_hole]
    my_score = HandEvaluator.eval_hand(hole_card, community_card)
    return 1 if my_score >= max(opponents_score) else 0


def convert_win_rate(win_rate,):
    if win_rate > 0.5:
        #raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
        if win_rate > 0.85:
            state = "high"
        elif win_rate > 0.75:
            # If it is likely to win, then raise by the minimum amount possible
            state = "mid_high"
        else:
            state = "mid"
    else:
        state = "low"
    return state


def get_table_values(table, win_rate, street):
    max_num = None
    index = 0
    for i, action in enumerate(table[street][win_rate]):
      #  if table[street][win_rate][action] !=  []:
        if max_num is None:
            max_num = table[street][win_rate][action]
            index = i
        elif max_num < table[street][win_rate][action]:
            max_num = table[street][win_rate][action]
            index = i
    return max_num, index

def get_action_amount(win_rate, street, check, valid_actions,table, action_counts):
    raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
    can_call = len([item for item in valid_actions if item['action'] == 'call']) > 0
    if can_call:
        # If so, compute the amount that needs to be called
        call_amount = [item for item in valid_actions if item['action'] == 'call'][0]['amount']
    else:
        call_amount = 0
    amount = 0
    get_amount = {"raise": raise_amount_options['max'], "call": call_amount , "fold": 0}
    # Check whether it is possible to call
    max_num, index = get_table_values(table, win_rate, street)

    acts = [action for action in table[street][win_rate]]
    action = acts[index]
    if amount is None:
        items = [item for item in valid_actions if item['action'] == action]
        amount = items[0]['amount']
    action_counts[street][win_rate][action] += 1
    check[street] = (win_rate, action)
    return action, get_amount[action], check, action_counts



def restart_check():
    check = {"preflop":
         ()
        ,
     "flop":
         ()
        ,
     "turn":
         ()
        ,
     "river":
         ()

     }
    return check

def initialize_table():
    table = {"preflop":{"low": {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                                  "mid": {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                                  "mid_high": {"fold": np.random.random_sample(),
                                               "raise": np.random.random_sample(),
                                               "call": np.random.random_sample()},
                                  "high": {"fold": np.random.random_sample() ,
                                           "raise": np.random.random_sample(),
                                           "call": np.random.random_sample()}
                                  },

                      "flop": {"low": {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                                  "mid": {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                                  "mid_high": {"fold": np.random.random_sample(),
                                               "raise": np.random.random_sample(),
                                               "call": np.random.random_sample()},
                                  "high": {"fold": np.random.random_sample() ,
                                           "raise": np.random.random_sample(),
                                           "call": np.random.random_sample()}
                                  },
                      "turn": {"low": {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                                  "mid": {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                                  "mid_high": {"fold": np.random.random_sample(),
                                               "raise": np.random.random_sample(),
                                               "call": np.random.random_sample()},
                                  "high": {"fold": np.random.random_sample() ,
                                           "raise": np.random.random_sample(),
                                           "call": np.random.random_sample()}
                                  },
                      "river": {"low": {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                                  "mid": {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                                  "mid_high": {"fold": np.random.random_sample(),
                                               "raise": np.random.random_sample(),
                                               "call": np.random.random_sample()},
                                  "high": {"fold": np.random.random_sample() ,
                                           "raise": np.random.random_sample(),
                                           "call": np.random.random_sample()}
                                  },


                      }
    return table
class TableBot(BasePokerPlayer):
    def __init__(self, name: str, reward_discount: float = 0.95, win_value: float = 1.0, draw_value: float = 0.0,
loss_value: float = -1.0, learning_rate: float = 0.01, training: bool = True):
        super().__init__()
        self.reward_discount = reward_discount
        self.win_value = win_value
        self.draw_value = draw_value
        self.loss_value = loss_value
        self.side = None
        self.board_position_log = []
        self.action_log = []
        self.next_max_log = []
        self.values_log = []
        self.name = name
        self.nn = QNetwork(name, learning_rate)

        self.training = training
        self.wins = 0
        self.losses = 0
        self.initial_stack = 0
        self.table = initialize_table()
        self.TABLESIZE = 6
        self.check = restart_check()
        self.counts = 0
        self.action_log = []
        self.values_log =[]
        self.action_counts = {"preflop":{"low": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid_high": {"fold": 0,
                                               "raise": 0,
                                               "call": 0},
                                  "high": {"fold": 0 ,
                                           "raise": 0,
                                           "call": 0}
                                  },

                      "flop": {"low": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid_high": {"fold":0,
                                               "raise": 0,
                                               "call": 0},
                                  "high": {"fold": 0 ,
                                           "raise": 0,
                                           "call": 0}
                                  },
                      "turn": {"low": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid_high": {"fold": 0,
                                               "raise": 0,
                                               "call": 0},
                                  "high": {"fold": 0 ,
                                           "raise":0,
                                           "call":0}
                                  },
                      "river": {"low": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "mid_high": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  "high": {"fold": 0,
                                          "raise": 0,
                                          "call": 0},
                                  }
                              }

    #in helper code it is a tic tac toe graph that is tracking how to make decisions so in this case we are using the neural network in declare action
         # it will need to learn when we update the tables
                #   TO-DO:// COLLECT NECESSARY VARIABLES TO PUT INTO A NEURAL NET
                            # INPUT TYPE:
    def calculate_targets(self) -> [np.ndarray]:
        """
        Based on the recorded moves, compute updated estimates of the Q values for the network to learn
        """
        game_length = len(self.action_log)
        targets = []
        print(game_length)

        for i in range(game_length):
            target = np.copy(self.values_log[i])

            target[self.action_log[i]] = self.reward_discount * self.next_max_log[i]
            targets.append(target)
          #  print(target)
        return targets

    def get_probs(self, input_pos: np.ndarray) -> ([float], [float]):
        """
        Feeds the feature vector `input_pos` which encodes a board state into the Neural Network and computes the
        Q values and corresponding probabilities for all moves (including illegal ones).
        :param input_pos: The feature vector to be fed into the Neural Network.
        :return: A tuple of probabilities and q values of all actions (including illegal ones).

        """
       # init_op = tf.initialize_all_variables()
        init_op = tf.global_variables_initializer()
        probs, qvalues = TFSN.get_session().run([self.nn.probabilities, self.nn.q_values], feed_dict={self.nn.input_positions: [input_pos]})
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())

        return probs[0], qvalues[0]

    def declare_action(self, valid_actions, hole_card, round_state):
        # Estimate the win rate
        win_rate = estimate_win_rate(100, self.num_players, hole_card, round_state['community_card'])
        state = convert_win_rate(win_rate)
        street = round_state["street"]


        action, amount, self.check, self.action_counts = get_action_amount(state, street, self.check, valid_actions, self.table, self.action_counts)

        #HOW TO MAKE THE NEURAL NET
            # needs to change to np array --> [[all the values for each round]]
        nn = [self.table[street][state][action]
              for street in self.table
              for state in self.table[street]
              for action in self.table[street][state]]
       # print(nn)
        nn_input = np.array(nn)

        probs, qvalues = self.get_probs(nn_input)
        qvalues - np.copy(qvalues)
        #print(qvalues)
      #  print(probs)
        # for index, p in enumerate(qvalues):
        #     if not board.is_legal(index):
        #         probs[index] = -1
        # Our next move is the one with the highest probability after removing all illegal ones.
        #[fold, raise, call]
        mov_ref = {0: "fold", 1: "raise", 2: "call"}
        move = np.argmax(probs)  # int
        if len(self.action_log) > 0:
            self.next_max_log.append(qvalues[move])

        self.action_log.append(move)
        self.values_log.append(qvalues)

        # print(move, probs)
        # print(action, amount)
        return mov_ref[move], amount

    def receive_game_start_message(self, game_info):
        self.num_players = game_info['player_num']



    def receive_round_start_message(self, round_count, hole_card, seats):
        self.initial_stack = get_stack_amount(self.uuid, seats)
        pass

    def receive_street_start_message(self, street, round_state):
        pass

    def receive_game_update_message(self, action, round_state):
        pass

    def receive_round_result_message(self, winners, hand_info, round_state):
        is_winner = self.uuid in [item['uuid'] for item in winners]
        orgwins = self.wins
        self.wins += int(is_winner)
        self.losses += int(not is_winner)
        stack = get_stack_amount(self.uuid, round_state['seats'])
        #if win then the new stack would bigger then the initial stack
        diff_stack = stack - self.initial_stack
        reward = self.loss_value
        if orgwins < self.wins:
            diff_stack = abs(diff_stack)
            reward = self.win_value
        #print(self.check, diff_stack)
        for i in self.check.keys():
            state = i
            if self.check[i] is not ():
               # print(i)
                rate, action = self.check[i]
             #   print("Before: ",   self.table[state][rate][action])
                self.table[state][rate][action] += diff_stack
               # print("After: ",   self.table[state][rate][action])
        self.check = restart_check()
        self.counts += 1
        nn = [self.table[street][state][action]
              for street in self.table
              for state in self.table[street]
              for action in self.table[street][state]]
        nn_input = np.array(nn).reshape(1, 48)
        self.next_max_log.append(reward)

        # If we are in training mode we run the optimizer.
        targets = self.calculate_targets()
      #  print()
        if self.training and targets is not []:
            # We calculate our new estimate of what the true Q values are and feed that into the network as
            # learning target

           # print("TRAINING")
            targets = np.array(targets)
            # We convert the input states we hae recorded to feature vectors to feed into the training.
            # nn_input = [self.board_state_to_nn_input(x) for x in self.board_position_log]

            # We run the training step with the recorded inputs and new Q value targets.
            print("Training")
           # print({self.nn.input_positions: nn_input, self.nn.target_input: targets})
            TFSN.get_session().run([self.nn.train_step],
                                   feed_dict={self.nn.input_positions: nn_input, self.nn.target_input: targets})

        # if self.counts is 10000 :
       # self.build_graph(name=)
        with open("TGB_endtable.pickle", "wb+") as out:
            pickle.dump(self.table, out)
        with open("TGB_counts.pickle", "wb+") as out:
            pickle.dump(self.action_counts, out)








def setup_ai():
    return TableBot()

