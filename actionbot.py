from pypokerengine.engine.hand_evaluator import HandEvaluator
from pypokerengine.players import BasePokerPlayer
from pypokerengine.utils.card_utils import _pick_unused_card, _fill_community_card, gen_cards
import pandas as pd
import numpy as np
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input
from keras.layers.merge import Add, Multiply
from keras.optimizers import Adam
import keras.backend as K

import tensorflow as tf

import random
from collections import deque
 #Some useful packages

def convert_done(round):
    if round == "river":
        return True
    else:
        return False

def estimate_win_rate(nb_player, hole_card, community_card=None):
    if not community_card: community_card = []

    # Make lists of Card objects out of the list of cards
    community_card = gen_cards(community_card)
    hole_card = gen_cards(hole_card)

    # Estimate the win count by doing a Monte Carlo simulation
    win_count = sum([montecarlo_simulation(nb_player, hole_card, community_card)    for _ in range(1000)])
  #  win_count = montecarlo_simulation(nb_player, hole_card, community_card)
    return 1.0 * win_count/1000


def montecarlo_simulation(nb_player, hole_card, community_card):
    # Do a Monte Carlo simulation given the current state of the game by evaluating the hands
    community_card = _fill_community_card(community_card, used_card=hole_card + community_card)
    unused_cards = _pick_unused_card((nb_player - 1) * 2, hole_card + community_card)
    opponents_hole = [unused_cards[2 * i:2 * i + 2] for i in range(nb_player - 1)]
    opponents_score = [HandEvaluator.eval_hand(hole, community_card) for hole in opponents_hole]
    my_score = HandEvaluator.eval_hand(hole_card, community_card)
    return 1 if my_score >= max(opponents_score) else 0

def convert_data( nb_player, hole_card, community_card):
    community_card = gen_cards(community_card)
    #community_card = _fill_community_card(community_card, used_card=hole_card + community_card)
    hole_card = gen_cards(hole_card)
    my_score = HandEvaluator.eval_hand(hole_card, community_card)
    env = np.array([my_score, nb_player])
   # print(env)
    return env
def new_state(hole_card, community_card, nb_player):
    community_card = gen_cards(community_card)
    # community_card = _fill_community_card(community_card, used_card=hole_card + community_card)
    my_score = HandEvaluator.eval_hand(hole_card, community_card)
    env = np.array([my_score, nb_player])

def get_stack_amount(uuid, seats):
    for dict in seats:
        if dict['uuid'] is uuid:
            return dict['stack']

# Estimate the ratio of winning games given the current state of the game
def estimate_win_rate(nb_simulation, nb_player, hole_card, community_card=None):
    if not community_card: community_card = []

    # Make lists of Card objects out of the list of cards
    community_card = gen_cards(community_card)
    hole_card = gen_cards(hole_card)

    # Estimate the win count by doing a Monte Carlo simulation
    win_count = sum([montecarlo_simulation(nb_player, hole_card, community_card) for _ in range(nb_simulation)])
    return 1.0 * win_count / nb_simulation


def evaluate_hand(hole_card, community_card):
    assert len(hole_card)==2 and len(community_card)==5
    hand_info = HandEvaluator.gen_hand_rank_info(hole_card, community_card)
    return {
            "hand": hand_info["hand"]["strength"],
            "strength": HandEvaluator.eval_hand(hole_card, community_card)
}

def montecarlo_simulation(nb_player, hole_card, community_card):

    # Do a Monte Carlo simulation given the current state of the game by evaluating the hands
    community_card = _fill_community_card(community_card, used_card=hole_card + community_card)

    unused_cards = _pick_unused_card((nb_player - 1) * 2, hole_card + community_card)
    opponents_hole = [unused_cards[2 * i:2 * i + 2] for i in range(nb_player - 1)]
    opponents_score = [HandEvaluator.eval_hand(hole, community_card) for hole in opponents_hole]
    my_score = HandEvaluator.eval_hand(hole_card, community_card)
    return 1 if my_score >= max(opponents_score) else 0


def convert_win_rate(win_rate,):
    if win_rate > 0.5:
        #raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
        if win_rate > 0.85:
            state = "high"
        elif win_rate > 0.75:
            # If it is likely to win, then raise by the minimum amount possible
            state = "mid_high"
        else:
            state = "mid"
    else:
        state = "low"
    return state


def get_table_values(table, win_rate):
    nums = [table[win_rate][action] for action in table[win_rate]]
    acts = [action for action in table[win_rate]]
    return (acts[nums.index(max(nums))])

def get_action_amount(win_rate, check, valid_actions,table, action_counts):
    raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
    can_call = len([item for item in valid_actions if item['action'] == 'call']) > 0
    if can_call:
        # If so, compute the amount that needs to be called
        call_amount = [item for item in valid_actions if item['action'] == 'call'][0]['amount']
    else:
        call_amount = 0
    amount = 0
    get_amount = {"raise": raise_amount_options['max'], "call": call_amount , "fold": 0}
    # Check whether it is possible to call
    action = get_table_values(table, win_rate )

    if amount is None:
        items = [item for item in valid_actions if item['action'] == action]
        amount = items[0]['amount']
    action_counts[win_rate][action] += 1
    check[win_rate] = (action)
    return action, get_amount[action], check, action_counts


def restart_check():
    check = {"low ": (),
                   "mid": (),
                  "mid_high" : (),
                    "high":
         ()

     }
    return check

def initialize_table():

    table = {"low":
                                        {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                "mid":
                                        {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                "mid_high":
                                        {"fold": np.random.random_sample(),
                                          "raise": np.random.random_sample(),
                                          "call": np.random.random_sample()},
                "high":
                                        {"fold": np.random.random_sample() ,
                                           "raise": np.random.random_sample(),
                                           "call": np.random.random_sample()}

                      }
    return table

class actionBot(BasePokerPlayer):
    def __init__(self, sess):
        super().__init__()
        #not needing env or session, because that gets handled in declare action
            # env --> hold-card and round_state
            #sess? -->
               # sess = tf.Session()
                #K.set_session(sess)
        self.sess = sess
        self.wins = 0
        self.losses = 0
        self.reward = 0
        self.learning_rate = 0.05
        self.epsilon = 1.0
        self.epsilon_decay = .995
        self.gamma = .95
        self.tau = .125
        self.memory = deque(maxlen=2000)
        self.actions = []
        self.cur_states = []
        self.table = initialize_table()
        self.check = restart_check()
        self.counts = 0
        self.action_counts = {"low": {"fold": 0,
                                      "raise": 0,
                                      "call": 0},
                              "mid": {"fold": 0,
                                      "raise": 0,
                                      "call": 0},
                              "mid_high": {"fold": 0,
                                           "raise": 0,
                                           "call": 0},
                              "high": {"fold": 0,
                                       "raise": 0,
                                       "call": 0}

                              }




    def declare_action(self, valid_actions, hole_card, round_state):
        # Estimate the win rate
        env = convert_data(self.num_players, hole_card, round_state["community_card"])
#        memory = convert_memory(round_state['action_histories'])
        #   == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == =  #
        #                               Actor Model                             #
        # Chain rule: find the gradient of chaging the actor network params in  #
        # getting closest to the final value network predictions, i.e. de/dA    #
        # Calculate de/dA as = de/dC * dC/dA, where e is error, C critic, A act #
        # ===================================================================== #
        #print("env: ", env)
        self.actor_state_input, self.actor_model = self.create_actor_model(env)
        _,self.target_actor_model = self.create_actor_model(env)

        self.actor_critic_grad = tf.placeholder(tf.float32,
                                                [None,      env.shape[
                                                    0]])  # where we will feed de/dC (from critic)

        self.actor_model_weights = self.actor_model.trainable_weights
        self.actor_grads = tf.gradients(self.actor_model.output,
                                        self.actor_model_weights, self.actor_critic_grad)  # dC/dA (from actor)
        grads = zip(self.actor_grads, self.actor_model_weights)
        self.optimize = tf.train.AdamOptimizer(self.learning_rate).apply_gradients(grads)
        #print(self.optimize)
        # ===================================================================== #
        #                              Critic Model                             #
        # ===================================================================== #

        self.critic_state_input, self.critic_action_input, self.critic_model = self.create_critic_model(env)
        _, _, self.target_critic_model = self.create_critic_model(env)

        self.critic_grads = tf.gradients(self.critic_model.output,
                                         self.critic_action_input)  # where we calcaulte de/dC for feeding above
        cur_state = env.reshape((1, env.shape[0]))
        #action = self.actor_critic.act(cur_state)
        self.cur_states.append(cur_state)
        action = self.act(cur_state, self.num_players, hole_card=hole_card, community_card=round_state['community_card'], actor_model = self.actor_model)


        win_rate = action
        self.actions.append(win_rate)
        print(win_rate)
        # Check whether it is possible to call
        can_call = len([item for item in valid_actions if item['action'] == 'call']) > 0
        if can_call:
            # If so, compute the amount that needs to be called
            call_amount = [item for item in valid_actions if item['action'] == 'call'][0]['amount']
        else:
            call_amount = 0

        amount = None

        # If the win rate is large enough, then raise
        if win_rate > 0.5:
            raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
            if win_rate > 0.85:
                # If it is extremely likely to win, then raise as much as possible
                action = 'raise'
                amount = raise_amount_options['max']
            elif win_rate > 0.75:
                # If it is likely to win, then raise by the minimum amount possible
                action = 'raise'
                amount = raise_amount_options['min']
            else:
                # If there is a chance to win, then call
                action = 'call'
        else:
            action = 'call' if can_call and call_amount == 0 else 'fold'

        # Set the amount
        if amount is None:
            items = [item for item in valid_actions if item['action'] == action]
            amount = items[0]['amount']
      #  print(estimate_win_rate(convert_data()))
        return action, amount

#    == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == =  #
    #                              Model Definitions                            #
    # ========================================================================= #

    def create_actor_model(self, env):
        state_input = Input(shape=env.shape)
        h1 = Dense(24, activation='relu')(state_input)
        h2 = Dense(48, activation='relu')(h1)
        h3 = Dense(24, activation='relu')(h2)
        output = Dense(env.shape[0], activation='sigmoid')(h3)
        #print("Output:", output)
        model = Model(input=state_input, output=output)
        adam = Adam(lr=0.001)
        model.compile(loss="mse", optimizer=adam)
        #print(model)
        return state_input, model

    def create_critic_model(self, env):
        state_input = Input(shape=env.shape)
        state_h1 = Dense(24, activation='relu')(state_input)
        state_h2 = Dense(48)(state_h1)

        action_input = Input(shape=env.shape)
        action_h1 = Dense(48)(action_input)

        merged = Add()([state_h2, action_h1])
        merged_h1 = Dense(24, activation='relu')(merged)
        output = Dense(1, activation='relu')(merged_h1)
        model = Model(input=[state_input, action_input], output=output)

        adam = Adam(lr=0.001)
        model.compile(loss="mse", optimizer=adam)
        return state_input, action_input, model

    # ========================================================================= #
    #                               Model Training                              #
    # ========================================================================= #

    def remember(self, cur_state, action, reward,new_state, done):
        self.memory.append([cur_state, action, reward, new_state, done])

    def _train_actor(self, samples, actor_model, critic_grads, critic_state_input,critic_action_input, actor_state_input, actor_critic_grad
                     , optimize):
        for sample in samples:
            cur_state, action, reward, done = sample
            predicted_action = actor_model.predict(cur_state)
            grads = self.sess.run(critic_grads, feed_dict={
                critic_state_input: cur_state,
                critic_action_input: predicted_action
            })[0]

            self.sess.run(optimize, feed_dict={
                actor_state_input: cur_state,
                actor_critic_grad: grads
            })

    def _train_critic(self, samples, target_actor_model, target_critic_model, critic_model):
        for sample in samples:
            print(sample)
            cur_state, action, reward, new_state, done = sample
            if not done:
                target_action = target_actor_model.predict(new_state)
                future_reward = target_critic_model.predict(
                    [new_state, target_action])[0][0]
                reward += self.gamma * future_reward
            critic_model.fit([cur_state, action], reward, verbose=0)

    def train(self, memory, actor_model, critic_model, critic_state_input,critic_grads, target_actor_model, target_critic_model, critic_action_input, actor_state_input, actor_critic_grad, optimize):
        batch_size = 32
        if len(memory) < batch_size:
            return
        samples = random.sample(memory, batch_size)
        self._train_critic(samples, target_actor_model, target_critic_model, critic_model)
        self._train_actor(self, samples, actor_model, critic_grads, critic_state_input,
                     critic_action_input, actor_state_input, actor_critic_grad, optimize)


    # ========================================================================= #
    #                         Target Model Updating                             #
    # ========================================================================= #

    def _update_actor_target(self):
        actor_model_weights = self.actor_model.get_weights()
        actor_target_weights = self.target_critic_model.get_weights()

        for i in range(len(actor_target_weights)):
            actor_target_weights[i] = actor_model_weights[i]
        self.target_critic_model.set_weights(actor_target_weights)

    def _update_critic_target(self):
        critic_model_weights = self.critic_model.get_weights()
        critic_target_weights = self.critic_target_model.get_weights()

        for i in range(len(critic_target_weights)):
            critic_target_weights[i] = critic_model_weights[i]
        self.critic_target_model.set_weights(critic_target_weights)

    def update_target(self):
        self._update_actor_target()
        self._update_critic_target()

    # ========================================================================= #
    #                              Model Predictions                            #
    # ========================================================================= #

    def act(self, cur_state, nb_player, hole_card, actor_model, community_card=None):
        self.epsilon *= self.epsilon_decay
        if np.random.random() < self.epsilon:
            return estimate_win_rate(nb_player=nb_player, hole_card=hole_card, community_card=community_card)
        pred = actor_model.predict(cur_state)
        return np.argmax(pred[0])

    def receive_game_start_message(self, game_info):
        self.num_players = game_info['player_num']

    def receive_round_start_message(self, round_count, hole_card, seats):
        pass

    def receive_street_start_message(self, street, round_state):
        pass

    def receive_game_update_message(self, action, round_state):
       # print(action, round_state)
        pass

    def receive_round_result_message(self, winners, hand_info, round_state):
        is_winner = self.uuid in [item['uuid'] for item in winners]
        orgwins = self.wins
        self.wins += int(is_winner)
        self.losses += int(not is_winner)
        self.losses += int(not is_winner)

        stack = get_stack_amount(self.uuid, round_state['seats'])

        # figures out how much to adjust training based on diff for stacks
        diff_stack = stack - self.initial_stack
        if orgwins < self.wins:
            diff_stack = abs(diff_stack)
        curr_state = [self.table[type][action] for type in self.table for action in self.table[type]]
        #curr_state = self.table
        # adjusting table amounts and keep tracking of global variabels
        for win_rate in self.check.keys():
            if self.check[win_rate] is not ():
                action = self.check[win_rate]  # dataframe --> {fold:(action took)}
                self.table[win_rate][action] += diff_stack
        self.check = restart_check()
        self.counts += 1
        new_state = [self.table[type][action] for type in self.table for action in self.table[type]]

        if orgwins < self.wins:
            self.reward = 1
        else:
            self.reward = 0
        done = False
        #change based on amount of rounds in simulation
        if round_state["round_count"] is 10000:
            done = True
        is_winner = self.uuid in [item['uuid'] for item in winners]
        orgwins = self.wins
        self.wins += int(is_winner)
        self.losses += int(not is_winner)
        action = np.array(self.actions)
        action = action.reshape((1, action.shape[0]))
        self.remember(np.array(curr_state), np.array(self.actions), reward=self.reward, new_state=new_state, done=done)
        self.train(self.memory, self.actor_model, self.critic_model, self.critic_state_input, self.critic_grads, self.target_actor_model,
                   self.target_critic_model, self.critic_action_input, self.actor_state_input, self.actor_critic_grad, self.optimize)
        # Initialize for later gradient calculations
        self.sess.run(tf.initialize_all_variables())
        self.actions = []
        self.cur_states = []


def setup_ai(sess):
    return actionBot(sess=sess)

