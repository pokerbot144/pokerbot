from callbot import CallBot
from general_tablebot import GeneralBot
from pypokerengine.api.game import setup_config, start_poker
from databloggerbot import DataBloggerBot
from tablebot import TableBot
from datetime import datetime
from actionbot import actionBot
import pickle
import numpy as np
import tensorflow as tf
from TFSessionManager import TFSessionManager as TFSN


def get_time():
    print(datetime.now().time())
if __name__ == '__main__':
    get_time()
    sess = tf.Session()
    call_bot = CallBot()
    table_bot = TableBot("QLearner1")
    blogger_bot = DataBloggerBot()
    general_bot = GeneralBot()
    act = actionBot(sess)
    stack_log1 = []
    stack_log2 = []
    stack_log3 = []
    #envir
    averages1= []
    stack_log4 = []
    TFSN.set_session(tf.Session())

    TFSN.get_session().run(tf.global_variables_initializer())
    writer = tf.summary.FileWriter('log', TFSN.get_session().graph)
    for round in range(10000):
        p1, p2= table_bot, blogger_bot
        config = setup_config(max_round=5, initial_stack=100, small_blind_amount=5)
        config.register_player(name="p1", algorithm=p1)
        config.register_player(name="p2", algorithm=p2)

        game_result = start_poker(config, verbose=0)

         #stack_log1.append([player['stack'] for player in game_result['players'] if player['uuid'] == general_bot.uuid])
        stack_log2.append([player['stack'] for player in game_result['players'] if player['uuid'] == table_bot.uuid])
        # stack_log3.append([player['stack'] for player in game_result['players'] if player['uuid'] == blogger_bot.uuid])
        # stack_log4.append([player['stack'] for player in game_result['players'] if player['uuid'] == call_bot.uuid])

        print('Avg. stack:', '%d' % (int(np.mean(stack_log2))))

    writer.close()
    TFSN.set_session(None)
    with open("tableNNP_blog", "wb") as out:
        pickle.dump(stack_log2, out)
    # with open("general_table_blog_stack.pickle", "wb") as out:
    #    # print(stack_log)
    #     pickle.dump(stack_log1, out)
    # with open("table_general_blog_stack.pickle", "wb") as out:
    #     # print(stack_log)
    #     pickle.dump(stack_log2, out)
    # with open("blog_general_table_stack.pickle", "wb") as out:
    #     # print(stack_log)
    #     pickle.dump(stack_log3, out)
    # with open("call_blog_general_table_stack.pickle", "wb") as out:
    #     # print(stack_log)
    #     pickle.dump(stack_log4, out)


    get_time()
  #  sess.close()
    #once we have all of this data we could then use it for a deep learning (due to having the simulations, which is what the robot is learning against)
    # create a function that does it with the stack